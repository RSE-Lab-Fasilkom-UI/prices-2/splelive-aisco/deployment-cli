from multiprocessing.pool import ThreadPool
from deployment import initiate_instance_objects, list_instance_names
from deployment.destroy import run_command as destroy_product
from deployment.start import start_non_nginx_instances
from utils.administration import (build_payload, check_product_exists,
                                  write_json_payload)
from utils.ports import assign_ports


def run_command(args, config):
    if check_product_exists(args, config):
        raise ValueError("Project %s already exists!" % (args.product_name, ))

    payload = build_payload(args, config)
    sockets = __assign_ports_to_payload(payload)
    write_json_payload(payload, config)
    instances = initiate_instance_objects(payload, config)

    nginx_build_success = instances["nginx"].build()
    if nginx_build_success:
        instances["nginx"].run(sockets["nginx"])

    build_success = nginx_build_success and build_instances(instances)
    if not build_success:
        return destroy_product(args, config)

    start_non_nginx_instances(instances, sockets)
    print("Deployment success! You can access the product on: http://localhost:%d" %
            (payload["ports"]["nginx"]))


def build_instances(instances):
    thread_pool = ThreadPool(processes=3)
    build_threads = {
        x: thread_pool.apply_async(instances[x].build)
        for x in instances.keys() if x != "nginx"
    }

    failed = []
    for key in build_threads:
        build_success = build_threads[key].get()
        if not build_success:
            failed.append(key)
            print("Build failed on instances: %s" % (key, ))
    return not failed


def __assign_ports_to_payload(payload):
    instance_names = list_instance_names(payload)
    sockets = assign_ports(instance_names)
    payload["ports"] = {x: y.port for x, y in sockets.items()}
    return sockets
