import os
import shutil
from deployment import initiate_instance_objects
from deployment.stop import stop_instances
from utils.administration import (check_product_exists, read_json_payload)
from utils.shell import backup_logs


def run_command(args, config):
    if not check_product_exists(args, config):
        raise FileNotFoundError(
            "Project %s not found. Please check deployment path in your config.ini!"
            % (args.product_name, ))

    payload = read_json_payload(args.product_name, config)
    instances = initiate_instance_objects(payload, config)

    stop_instances(instances)
    backup_logs(payload, config)
    __remove_product(payload, config, instances)


def __remove_product(payload, config, instances):
    DEPLOY_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))

    for name in instances:
        instances[name].destroy()
    shutil.rmtree(os.path.join(DEPLOY_ROOT, payload["product_name"]))
