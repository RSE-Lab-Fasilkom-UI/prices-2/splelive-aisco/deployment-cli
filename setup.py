#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from setuptools import find_packages, setup

CURRENT_PYTHON = sys.version_info[:2]
REQUIRED_PYTHON = (3, 5)
version = "0.0.1"

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

with open("requirements.txt", "r") as requirements_file:
    requirements = requirements_file.read().strip().split("\n")

if CURRENT_PYTHON < REQUIRED_PYTHON:
    sys.stderr.write("""
==========================
Unsupported Python version
==========================
The usage of this tool require Python {}.{}, but you"re trying to
install it on Python {}.{}.
Please use the requested Python version or more to use this tool properly
""".format(*(REQUIRED_PYTHON + CURRENT_PYTHON)))
    sys.exit(1)

setup(
    name="ABS + IFML Deployment Tool",
    version=version,
    python_requires=">={}.{}".format(*REQUIRED_PYTHON),
    url="http://www.aiscoweb.org/",
    author=
    "Reliable Software Engineering Lab, Faculty of Computer Science, University of Indonesia",
    author_email="rse@cs.ui.ac.id",
    description=
    ("A simple tool to deploy generated Java and Angular/React code using ABS and IFML."
     ),
    long_description=readme,
    license="BSD",
    packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
    include_package_data=True,
    scripts=["main/bin/itap-cli.py"],
    entry_points={
        "console_scripts": [
            "django-admin = django.core.management:execute_from_command_line",
        ]
    },
    install_requires=requirements,
    zip_safe=False,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Natural Language :: Indonesian",
        "Operating System :: Unix",
        "Operating System :: POSIX :: Linux",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Java",
        "Programming Language :: JavaScript",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Software Development :: Code Generators",
        "Topic :: Utilities",
    ],
    project_urls={
        "Source": "https://gitlab.com/ichlaffterlalu/abs-ifml-deployment-cli",
    },
)
