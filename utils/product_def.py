import json
import os
import re


class FeatureDiagram:
    def __init__(self, filename):
        if filename.endswith(".json"):
            self.__tree = self.__read_diagram_json(filename)
        else:
            self.__tree = self.__read_diagram_abs(filename)

    def __read_diagram_abs(self, filename):
        try:
            diagram_path = os.path.abspath(os.path.expanduser(filename))
            with open(diagram_path, "r") as diagram_file:
                diagram_raw = diagram_file.read()
            diagram_raw = re.sub(r"\s+", " ", diagram_raw).strip()
            root = re.search(r'root (.*?)\{(.*)\}', diagram_raw)
            productline_name = root.group(1).strip()
            model_root = {"productline_name": productline_name, "children": []}
            return self.__read_diagram_abs_rec(model_root, root.group(2))
        except (IOError, OSError):
            return {}

    def __read_diagram_abs_rec(self, sub_dict, diagram_raw):
        sub_dict = dict(sub_dict)  # Create new copy
        while diagram_raw != "":
            diagram_raw = diagram_raw.strip()
            if diagram_raw.startswith("group"):
                group_raw = re.search(r"group (.*?)\{(.*)\}", diagram_raw)
                group_type = group_raw.group(1).strip()
                content = group_raw.group(2).strip()
                if group_type.startswith("allof"):
                    sub_dict["relation"] = {}
                elif group_type.startswith("oneof"):
                    sub_dict["relation"] = {"min": 1, "max": 1}
                elif group_type.startswith("["):
                    raw_constraint = re.search(
                        r"\[([0-9]+|\*?)\.\.([0-9]+|\*?)\]", group_type)
                    minimum = raw_constraint.group(1).strip()
                    if minimum != "*":
                        minimum = int(minimum)
                    maximum = raw_constraint.group(2).strip()
                    if maximum != "*":
                        maximum = int(maximum)
                    violation_cond1 = minimum == "*" and isinstance(
                        maximum, int)
                    violation_cond2 = isinstance(minimum, int) and isinstance(
                        maximum, int) and minimum > maximum
                    if violation_cond1 or violation_cond2:
                        raise SyntaxError("Invalid syntax on: " +
                                          raw_constraint.group(0))
                sub_dict["children"] = sub_dict.get(
                    "children", []) + self.__read_diagram_abs_rec_child(
                        [], content)
                diagram_raw = diagram_raw.replace(group_raw.group(0), "")
            elif diagram_raw.startswith("require:"):
                require_raw = re.search(r"require:(.*?);+", diagram_raw)
                require = require_raw.group(1).strip()
                sub_dict["requires"] = sub_dict.get("requires", []) + [require]
                diagram_raw = diagram_raw.replace(require_raw.group(0),
                                                  "").strip()
            elif diagram_raw.startswith("exclude:"):
                exclude_raw = re.search(r"exclude:(.*?);+", diagram_raw)
                exclude = exclude_raw.group(1).strip()
                sub_dict["excludes"] = sub_dict.get("excludes", []) + [exclude]
                diagram_raw = diagram_raw.replace(exclude_raw.group(0),
                                                  "").strip()
            else:
                raise SyntaxError("Invalid syntax on: " + diagram_raw)
        return sub_dict

    def __split_diagram_abs_child(self, diagram_raw):
        children_raw = []
        stack = 0
        prev_end = -1
        for i, char in enumerate(diagram_raw):
            if char == "{":
                stack += 1
            elif char == "}":
                stack -= 1
            elif char == "," and stack == 0:
                child_raw = diagram_raw[prev_end + 1:i]
                if child_raw != "":
                    children_raw.append(child_raw)
                prev_end = i
        last_child_raw = diagram_raw[prev_end + 1:]
        if last_child_raw != "":
            children_raw.append(last_child_raw)
        return children_raw

    def __read_diagram_abs_rec_child(self, child_list, diagram_raw):
        child_list = list(child_list)  # Create new copy
        diagram_raw = diagram_raw.strip()
        children_raw = self.__split_diagram_abs_child(diagram_raw)
        for child_raw in children_raw:
            if child_raw.strip().startswith("opt "):
                mandatory = False
                child_raw = child_raw[5:].strip()
            else:
                mandatory = True
            child_re = re.search(r"((.*?)(\{(.*)\})|(.*))", child_raw.strip())
            if child_re.group(2) is None:
                child_list.append({
                    "name": child_re.group(1).strip(),
                    "mandatory": mandatory,
                    "relation": {},
                    "children": []
                })
            else:
                child_list.append(
                    self.__read_diagram_abs_rec(
                        {
                            "name": child_re.group(2).strip(),
                            "mandatory": mandatory,
                            "relation": {},
                            "children": []
                        }, child_re.group(4)))
        return child_list

    def __read_diagram_json(self, filename):
        try:
            diagram_path = os.path.abspath(os.path.expanduser(filename))
            diagram_file = open(diagram_path, "r")
            diagram_json = diagram_file.read()
            diagram_file.close()
            return json.loads(diagram_json)
        except (IOError, OSError, json.JSONDecodeError):
            return {}

    def get_diagram(self):
        return self.__tree

    def expand_default(self, user_feature_dict, root_feature_name=None):
        tree = self.__tree["children"]
        if root_feature_name:  # trim dicts to a root feature
            user_feature_dict = user_feature_dict[root_feature_name]
            for model in tree:
                if model["name"] == root_feature_name:
                    tree = model
                    break
        result = self.__expand_default_rec(user_feature_dict, tree)
        result = self.__verify_require_exclude(result)
        return result

    def __expand_default_rec(self, sub_user, sub_diagram):
        result = []
        # If it's string, make sure it's default. Derive the default and its children.
        if isinstance(sub_user, str) and sub_user == "default":
            for subfeature in sub_diagram:
                if subfeature["mandatory"]:
                    result.append(subfeature["name"])
                    result.extend(
                        self.__expand_default_rec("default",
                                                  subfeature["children"]))
                    result.extend(subfeature.get("requires", []))
                    result.extend(
                        ["-" + x for x in subfeature.get("excludes", [])])
        # If it's list, derive defaults for each feature in the list.
        elif isinstance(sub_user, list):
            for subfeature in sub_diagram:
                if subfeature["name"] in sub_user:
                    result.append(subfeature["name"])
                    result.extend(
                        self.__expand_default_rec("default",
                                                  subfeature["children"]))
                    result.extend(subfeature.get("requires", []))
                    result.extend(
                        ["-" + x for x in subfeature.get("excludes", [])])
        # If it's dict, derive defaults for each feature's children.
        elif isinstance(sub_user, dict):
            for subfeature in sub_diagram:
                if subfeature["name"] in sub_user.keys():
                    result.append(subfeature["name"])
                    result.extend(
                        self.__expand_default_rec(sub_user[subfeature["name"]],
                                                  subfeature["children"]))
                    result.extend(subfeature.get("requires", []))
                    result.extend(
                        ["-" + x for x in subfeature.get("excludes", [])])
        return result

    def __verify_require_exclude(self, feature_list):
        feature_set = set()
        exclude_set = set()
        for feature in feature_list:
            if feature.startswith("-"):  # "-" sign for excludes
                exclude_set.add(feature[1:])
            else:
                feature_set.add(feature)
        for excluded in exclude_set:
            if excluded in feature_set:
                raise TypeError(
                    "Excluded feature %s exists in product definition." %
                    (excluded, ))
        return list(feature_set)


def read_basic_feature_config(config, product_name):
    try:
        PRODUCT_CONFIG_ROOT = os.path.abspath(
            os.path.expanduser(config["work_paths::product_config_location"]))
        basic_features_path = os.path.join(PRODUCT_CONFIG_ROOT, product_name,
                                           "basic.json")
        with open(basic_features_path, "r") as file_:
            basic_features_json = file_.read()
        return json.loads(basic_features_json)
    except (IOError, OSError, json.JSONDecodeError):
        return {}


def read_advanced_feature_config(config, product_name, feature_name):
    try:
        PRODUCT_CONFIG_ROOT = os.path.abspath(
            os.path.expanduser(config["work_paths::product_config_location"]))
        advanced_features_path = os.path.join(PRODUCT_CONFIG_ROOT,
                                              product_name,
                                              feature_name.lower() + ".json")
        with open(advanced_features_path, "r") as file_:
            advanced_features_json = file_.read()
        return json.loads(advanced_features_json)
    except (IOError, OSError, json.JSONDecodeError):
        return []
